package fr.inrae.toulouse.metexplore.tutorialmet4j.met4j_io;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.JsbmlReader;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.Met4jSbmlReaderException;

import java.io.IOException;

public class UseJsbmlReader {

    public static void main(String[] args) throws IOException, Met4jSbmlReaderException {

        /**
         * Inits reader
         */

        String inputFile = "src/main/resources/sbmls/iAF1260.xml";
        JsbmlReader reader = new JsbmlReader(inputFile);

        /**
         * If your SBML follows the last specifications,
         * this should be enough
         */
        BioNetwork network = reader.read();

        /**
         * Otherwise... It's more tricky, examples will come later....
         */


    }

}
