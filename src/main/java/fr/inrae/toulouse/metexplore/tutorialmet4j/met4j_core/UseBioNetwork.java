package fr.inrae.toulouse.metexplore.tutorialmet4j.met4j_core;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.*;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.utils.BioReactionUtils;

import java.util.HashSet;
import java.util.Set;

public class UseBioNetwork {

    public static void main(String[] args) {

        /**
         * Creation of a BioNetwork
         *
         * The BioNetwork class is the essential class of met4j.
         *
         * It contains and links all the entities composing a metabolic network (metabolites, reactions,
         * pathways, genes, proteins, enzymes, compartments)
         */
        BioNetwork network = new BioNetwork("myBioNetwork");

        /**
         * myBioNetwork is the id of the network. If you don't specify the id, a unique id will be set as id.
         */

        /**
         * Adding an element in a BioNetwork
         *
         * The method "add" of the BioNetwork class allows to add any entity cited above in a metabolic network.
         */


        /**
         * Ex : Add a metabolite
         */
        BioMetabolite m1 = new BioMetabolite("m1", "M1");
        network.add(m1);

        /**
         * Ex : Add a reaction
         */
        BioReaction r1 = new BioReaction("r1");
        network.add(r1);


        /**
         * Be careful, if you add an entity with the same id
         * than an entity previously added, this one will
         * be replaced by that one
         * The following lines would throw an exception
         */
        // BioMetabolite m1Bis = new BioMetabolite("m1", "M1Bis");
        // network.add(m1Bis);


        /**
         * Be careful, if you add an entity with the same id
         * than an entity previously added, this one will
         * be replaced by that one
         */

        /**
         * Checks if an entity is present in the BioNetwork
         * The method "contains" of the BioNetwork class allows to check the presence of any entity cited
         *  above in a metabolic network.
         *
         *  Let's continue with the same examples.
         */
        network.add(m1);
        System.err.println("Does network contains m1 ?\n" + network.contains(m1));

        BioMetabolite m2 = new BioMetabolite("m2");
        System.err.println("Does network contains m2 ?\n" + network.contains(m2));

        /**
         * Affect a metabolite to a compartment
         * Be careful, the metabolite and the compartment
         * must be added before in the network
         */
        BioCompartment c1 = new BioCompartment("c1", "compartment1");
        network.add(c1);

        network.affectToCompartment(c1, m1);

        /**
        * Affect several metabolites to a compartment
        */
        network.add(m2);
        network.affectToCompartment(c1, m1, m2);


        BioCollection<BioMetabolite> c = new BioCollection<>();
        c.add(m1);
        c.add(m2);
        network.affectToCompartment(c1,c);

        /**
         * Building of a reaction
         *
         * Let's say that we want to build this reaction:
         * R1 : 2 m1 + 3 m2 -> m3 + m4
         *
         */
        BioMetabolite m3 = new BioMetabolite("m3");
        BioMetabolite m4 = new BioMetabolite("m4");
        network.add(m3, m4);
        network.affectToCompartment(c1, m3, m4);
        network.affectLeft(r1, 2.0, c1, m1);
        network.affectLeft(r1, 3.0, c1, m2);
        network.affectRight(r1, 1.0, c1, m3);
        network.affectRight(r1, 1.0, c1, m4);

        /**
         *Since the stoichiometric coefficients are the same for m3 and m4,
         * it was possible to do:
         */
        network.affectRight(r1, 1.0, c1, m3, m4);

        /**
         * Or
         */
        BioCollection<BioMetabolite> rightCollection = new BioCollection<>();
        rightCollection.add(m3, m4);


        /**
         * By using BioReactants
         */

        BioReaction r2 = new BioReaction("r2");
        network.add(r2);
        network.affectLeft(r2, 1.0, c1, m1, m2);
        network.affectRight(r2, 2.0, c1, m3);

        /**
         * Link a gene to a protein
         */
        BioGene g1 = new BioGene("g1");
        BioProtein p1 = new BioProtein("p1");
        network.add(g1, p1);
        network.affectGeneProduct(p1, g1);

        /**
         * Link proteins to an enzyme
         */
        BioEnzyme e1 = new BioEnzyme("e1");
        BioProtein p2 = new BioProtein("p2");
        network.add(e1, p2);
        BioCollection<BioProtein> components = new BioCollection<>();
        components.add(p1, p2);
        network.affectSubUnit(e1, 1.0, components);

        /**
         * Assign enzymes to a reaction
         */
        BioEnzyme e2 = new BioEnzyme("e2");
        BioProtein p3 = new BioProtein("p3");
        network.add(e2, p3);
        network.affectSubUnit(e2, 1.0, p3);
        network.affectEnzyme(r1, e2, e1);

        /**
         * Add reactions to pathways
         */
        BioPathway pathway1 = new BioPathway("p1");
        network.add(pathway1);
        network.affectToPathway(pathway1, r1, r2);

        /**
         * Get collection of metabolites
         */
        /**
         * All the metabolites
         */
        BioCollection<BioMetabolite> allMetabolites = network.getMetabolitesView();


        /**
         * Metabolites involved in reactions
         */
        BioCollection<BioReaction> reactions = new BioCollection<>();
        reactions.add(r1, r2);
        BioCollection<BioMetabolite> metabolitesInvolvedInR1R2 = network.getMetabolitesFromReactions(reactions);


        /**
         * Metabolites involved in a pathway
         */
        BioCollection<BioMetabolite> metabolitesInvolvedInPathways = network.getMetabolitesFromPathway(pathway1);

        /**
         * Get Left or Right metabolites in a reaction
         */
        BioCollection<BioMetabolite> lefts = network.getLefts(r1);
        BioCollection<BioMetabolite> rights = network.getRights(r1);

        /**
         * Or to have stoichiometry:
         */
        BioCollection<BioReactant> leftReactants = network.getLeftReactants(r1);
        BioCollection<BioReactant> rightReactants = network.getRightReactants(r1);

        /**
         * Get reactions
         */

        /**
         * All reactions
         */
        BioCollection<BioReaction> allReactions = network.getReactionsView();

        /**
         * Reactions from genes
         */


        /**
         * Get reactions that involves one gene
         */
        BioCollection<BioReaction> reactionsWithG1 = network.getReactionsFromGene(g1);
        System.err.println(reactionsWithG1);

        // reactionsWithG1 contains only G1

        /**
         * Get reactions that involves at least one of the genes of the collection
         */
        BioGene g2 = new BioGene("g2");
        network.add(g2);
        BioCollection<BioGene> g1g2 = new BioCollection<>();
        g1g2.add(g1, g2);
        BioCollection<BioReaction> reactionsWithG1orG2 = network.getReactionsFromGenes(g1g2, false);
        System.err.println(reactionsWithG1orG2);
        // reactionsWithG1orG2 contains only G1

        /**
         * Get reactions that involve at least all the genes of the collection
         */
        BioCollection<BioReaction> reactionsWithG1andG2 = network.getReactionsFromGenes(g1g2, true);
        System.err.println(reactionsWithG1andG2);
        // reactionsWithG1andG2 is empty

        /**
         * Get reactions from a metabolite
         */
        BioCollection<BioReaction> reactionsWithM1 = network.getReactionsFromMetabolite(m1);

        /**
         * Get reactions from pathways
         */
        BioCollection<BioPathway> pathways = new BioCollection<>();
        pathways.add(pathway1);
        BioCollection<BioReaction> reactionsInPathway1 = network.getReactionsFromPathways(pathways);


        /**
         * Get reactions that can produce a collection of metabolites
         */
        BioCollection<BioMetabolite> metabolites = new BioCollection<BioMetabolite>();
        metabolites.add(m3);
        metabolites.add(m4);
        BioCollection<BioReaction> reactionsProducingM3M4 = network.getReactionsFromProducts(metabolites, false);
        // reactionsProducingM3M4 contains r1
        metabolites.add(m1);
        BioCollection<BioReaction> reactionsProducingM3M4M1  = network.getReactionsFromProducts(metabolites, false);
        // reactionsProducingM3M4M1 is empty
        metabolites.clear();
        metabolites.add(m3);
        BioCollection<BioReaction> reactionsProducingAtLeastM3 = network.getReactionsFromProducts(metabolites, false);
        // reactionsProducingM3 contains r1 and r2
        BioCollection<BioReaction> reactionsProducingOnlyM3 = network.getReactionsFromProducts(metabolites, true);
        // reactionsProducingM3 is empty : there is no reaction producing only M3

       /**
        * Get reactions that can use a collection of metabolites
        */
       metabolites.clear();
       metabolites.add(m1);
       metabolites.add(m2);
       BioCollection<BioReaction> reactionsUsingM1M2 = network.getReactionsFromSubstrates(metabolites, false);


        /**
         * Get all pathways
         */
        BioCollection<BioPathway> allPathways = network.getPathwaysView();

        /**
         * Get Pathways from genes
         */
        /**
         * Get pathways that contains at least G1 or G2
         */
        BioCollection<BioPathway> pathwaysContainingG1orG2 = network.getPathwaysFromGenes(g1g2, false);
        /**
         * Get pathways that contains at least G1 or G2
         */
        BioCollection<BioPathway> pathwaysContainingG1andG2 = network.getPathwaysFromGenes(g1g2, true);

        /**
         * Get pathways from metabolites
         */
        metabolites.clear();
        metabolites.add(m1);
        metabolites.add(m2);
        BioCollection<BioPathway> pathwaysContainingM1OrM2 = network.getPathwaysFromMetabolites(metabolites, false);
        BioCollection<BioPathway> pathwaysContainingM1AndM2 = network.getPathwaysFromMetabolites(metabolites, true);

        /**
         * Get Pathways from reactions
         */
        BioCollection<BioReaction> r1r2 = new BioCollection<>();
        BioCollection<BioPathway> pathwaysContainingR1orR2 = network.getPathwaysFromReactions(r1r2, false);
        BioCollection<BioPathway> pathwaysContainingR1andR2 = network.getPathwaysFromReactions(r1r2, true);

        /**
         * Get Enzymes
         */
        BioCollection<BioEnzyme> allEnzymes = network.getEnzymesView();

        /**
         * Get proteins
         */
        BioCollection<BioProtein> allProteins = network.getProteinsView();


        /**
         * Get Genes
         */
        BioCollection<BioGene> allGenes = network.getGenesView();

        /**
         * Get genes involved in enzymes
         */
        BioCollection<BioGene> genesInvolvedInE1 = network.getGenesFromEnzyme(e1);
        BioCollection<BioEnzyme> e1e2 = new BioCollection<>();
        BioCollection<BioGene> genesInvolvedInE1AndE2 = network.getGenesFromEnzymes(e1e2);

        /**
         * Get genes involved in pathways
         */
        BioCollection<BioGene> genesInvolvedInPathway1 = network.getGenesFromPathways(pathways);

        /**
         * Get genes involved in reactions
         */
        BioCollection<BioGene> genesInvolvedInR1AndR2 = network.getGenesFromReactions(r1r2);

        /*
         Remove elements from a BioNetwork

        The method "removeOnCascade"  allows to remove several elements from a network.
        The OnCascade suffix means that related elements could be removed when some elements are removed.

        Remove on cascade a metabolite

                - The metabolite is removed from each of the reactions where it occurs
                - If the reaction does not contain metabolites anymore, it is also removed
                - It is removed from each compartment where it occurs. If the compartment does not contain
        any component anymore, it is removed.
                - It is removed from each enzyme where it occurs. If the enzyme does not contain any enzyme,
        it is removed
        */

        /**
         * Example :
         */
        network.removeOnCascade(m1);

        /**
         * If you remove m1, m2, m3 and m4, the reactions r1, r2 and the compartment c1
         * will be also removed on cascade
         *
         * Try :

        System.err.println("List of reactions before removing m1, m2, m3, m4 : "+network.getReactionsView());
        System.err.println("List of compartments before removing m1, m2, m3, m4 : "+network.getCompartmentsView());
        network.removeOnCascade(m2, m3, m4);
        System.err.println("List of reactions after removing m1, m2, m3, m4 : "+network.getReactionsView());
        System.err.println("List of compartments after removing m1, m2, m3, m4 : "+network.getCompartmentsView());
         */

        /**
        Remove on cascade a protein

                - It is removed from each enzyme where it occurs. If the enzyme does not contain any enzyme,
        it is removed
                - It is removed from each compartment where it occurs. If the compartment does not contain
        any component anymore, it is removed

         */

        System.err.println("List of enzymes before removing p1 :"+network.getEnzymesView());
        network.removeOnCascade(p1);
        System.err.println("List of enzymes after removing p1 :"+network.getEnzymesView());



        /**

        Remove on cascade a gene
                - Remove proteins coded by this gene

         */

        System.err.println("List of proteins before removing g2 :"+network.getProteinsView());
        network.removeOnCascade(g2);
        System.err.println("List of proteins after removing g2 :"+network.getProteinsView());

        /*
        Remove on cascade a reaction
                - Is removed from each pathway where it occurs.
                If the pathway is empty after that, the pathway
        is also removed
        */
        network.removeOnCascade(r1);

        /*
        Remove on cascade a compartment
                - Each reaction involving a metabolite in this compartment is removed
        */
        network.removeOnCascade(c1);

        return;


    }
}
