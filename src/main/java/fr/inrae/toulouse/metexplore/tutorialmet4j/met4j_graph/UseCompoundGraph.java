package fr.inrae.toulouse.metexplore.tutorialmet4j.met4j_graph;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioMetabolite;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioReaction;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_graph.computation.connect.weighting.DegreeWeightPolicy;
import fr.inrae.toulouse.metexplore.met4j_graph.computation.connect.weighting.WeightUtils;
import fr.inrae.toulouse.metexplore.met4j_graph.computation.transform.EdgeMerger;
import fr.inrae.toulouse.metexplore.met4j_graph.computation.transform.VertexContraction;
import fr.inrae.toulouse.metexplore.met4j_graph.computation.connect.weighting.WeightsFromFile;
import fr.inrae.toulouse.metexplore.met4j_graph.computation.utils.ComputeAdjacencyMatrix;
import fr.inrae.toulouse.metexplore.met4j_graph.core.WeightingPolicy;
import fr.inrae.toulouse.metexplore.met4j_graph.core.compound.CompoundGraph;
import fr.inrae.toulouse.metexplore.met4j_graph.core.compound.ReactionEdge;
import fr.inrae.toulouse.metexplore.met4j_graph.io.Bionetwork2BioGraph;
import fr.inrae.toulouse.metexplore.met4j_graph.io.ExportGraph;
import fr.inrae.toulouse.metexplore.met4j_graph.io.NodeMapping;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.JsbmlReader;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.Met4jSbmlReaderException;
import fr.inrae.toulouse.metexplore.met4j_mathUtils.matrix.ExportMatrix;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;

import java.io.IOException;
import java.util.List;
import java.util.Set;


/*
 * This is a tutorial for the use of the Met4J library for graph-based analysis of metabolic networks
 */
public class UseCompoundGraph {

    public static void main(String[] args) throws IOException, Met4jSbmlReaderException {

        String inputSbmlFile = "src/main/resources/sbmls/iAF1260.xml";;
        String outputDir = "target/";;
        String sideCompoundFile = "src/main/resources/others/iAF1260-side_compounds.tab";;

        /*
        I. CREATING METABOLIC GRAPHS
        ============================

        Importing a metabolic network from a SBML file
        ----------------------------------------------
        */
        System.out.println("importing network...");
        JsbmlReader reader = new JsbmlReader(inputSbmlFile);
            //The JsbmlReader parse the SBML content and store all of its data onto a BioNetwork object.
            //The BioNetwork object contains references to all bio-entities described in a metabolic model, and handle the connections between them.
        BioNetwork network = reader.read();



        /*
        Creating a compound graph
        -------------------------
        SBML is structured as a list of species (which include compounds), and a list of reactions that reference species as their inputs/outputs.
        Thus, direct mapping onto a graph object leads to a bipartite graph with two groups of nodes, with only inter-group links and no intra-group ones.
        It is usually more convenient to perform structural analysis on a simpler representation with one kind of node: the compound graph.
        The following code translates the SBML content as a compound-to-compound graph, where a reaction is represented by multiple substrate-product transition edges.
        */
        System.out.println("creating compound graph...");
        Bionetwork2BioGraph builder = new Bionetwork2BioGraph(network);
            //The Bionetwork2BioGraph class creates metabolic graph objects from the BioNetwork that store all of the sbml content.
            //Multiple graph types can be defined, such as compound graph, bipartite graph or reaction graph.
            //The created BioGraph is independent of the BioNetwork it has been created from, and support modifications such as addition/deletion of edges and nodes.
        CompoundGraph graph = builder.getCompoundGraph();
        System.err.println("nodes : "+graph.vertexSet().size());
        System.err.println("edges : "+graph.edgeSet().size());


        /*
        II. PROCESSING METABOLIC GRAPHS
        ===============================

        Side compound removal (recommended)
        -----------------------------------
        Metabolic networks contain compounds that are involved in reactions without being at the core of their activity.
        Such compounds are called side compounds or auxiliary compounds and include, for example, water or ATP.
        They tend to form irrelevant connexions (i.e. non-specific to any biological process), and as they're often ubiquitous, create many shortcuts
        that create spurious proximity between unrelated compounds.
        The following code imports a list of identifiers of side compounds and filter them out of the graph.
        */
        System.out.println("locating side compounds...");
        NodeMapping nodeLoader = new NodeMapping(graph).skipIfNotFound();
            //The NodeMapping class allows to retrieve nodes in a metabolic graph from a list of identifiers (in a file or directly by passing a Collection object)
            //Different handling of missing nodes can be set, such as skipping, creating them on the fly, or throw an exception.
        BioCollection<BioMetabolite> sidecompounds = nodeLoader.map(sideCompoundFile);

        System.out.println("removing side compounds...");
        boolean removed = graph.removeAllVertices(sidecompounds);
        if (removed) System.err.println(sidecompounds.size()+" compounds removed.");
        System.err.println("nodes : "+graph.vertexSet().size());
        System.err.println("edges : "+graph.edgeSet().size());


        /*
        Setting weights (optional)
        --------------------------
        It is common to attribute values to edges, for example, to account for their relative 'strength' during community detection
        or their 'cost' during path search. Met4J propose multiple weighting procedures that have been used in the context of metabolic networks,
        such as chemical similarity or incoming node degree.
        */
        System.out.println("applying weight...");
        WeightingPolicy wp = new DegreeWeightPolicy();
        wp.setWeight(graph);
        // weights can also be exported and imported from a file, using the following:
        //```
        //WeightUtils.export(graph, weightFilePath);
        //wp = new WeightsFromFile(weightFilePath, true);
        //```


        /*
        Merging compartments (optional)
        -------------------------------
        As metabolic models in SBML are mainly used for quantitative analysis rather that topological ones, the represented entities
        do not strictly represent metabolites, but rather pools of available metabolites of a given type, at a given location.
        Consequently, a compound may be duplicated for each cellular compartment. There are instances where such distinction may not be
        suited, where one would want one node to represent exclusively one compound.
        The following code merges compounds sharing the same name as a single node, effectively merging compartments.
         */
        System.out.println("merging compartment...");
        VertexContraction decomp = new VertexContraction();
            //The VertexContraction class allows to merge nodes according to a method passed as parameter, which return a unique string for each node.
            //If two or more nodes share the same string, then they will be merged as one.
            //Common methods are provided as static, such as using names, inchi, or a regex to extract identifier name base
            // (since compartments are often specified in identifiers as suffix)
        graph = decomp.decompartmentalize(graph, new VertexContraction.MapByName());
        System.err.println("nodes : "+graph.vertexSet().size());
        System.err.println("edges : "+graph.edgeSet().size());

        /*
        Creating simple graph (optional)
        --------------------------------
        Several reactions can perform the same substrate-product transition. Thus, compound graphs are usually "multi-graphs", which contains
        edges that connect the same nodes (parallel edges). Multi-graphs are not supported by all graph-analysis platforms, especially those relying on
        two-dimensional adjacency matrices. The following code merges parallel edges as a single one.
         */

        System.out.println("merging parallel edges...");
        CompoundGraph finalGraph = graph;
        EdgeMerger.mergeEdgesWithOverride(graph, EdgeMerger.lowWeightFirst(graph));
        //The EdgeMerger can be set using a comparator to choose which edge to keep in a set of parallel ones.
        //An alternative merge with no override remove all parallel edges and replace them with a new one,
        // with aggregated labels and bearing the sum of individual edges weights.
        System.err.println("nodes : "+graph.vertexSet().size());
        System.err.println("edges : "+graph.edgeSet().size());

        /*
        Editing metabolic graph
        -----------------------
        If the object of study requires diverging from the original network as described in the SBML, the metabolic graph can be manipulated
        by adding or removing edges, for example. Such modification will only affect the structure of the graph, no assumption
        regarding the biological coherency will be made. For example, two metabolites could be linked by an edge referencing a biochemical reaction
        that do not involve the former metabolites. Modifications of the model itself, that would abide to such constraints,
        could be made on the BioNetwork object prior to graph creation.
         */
        System.out.println("editing graph...");
        //EXAMPLES:

        //disconnect a compound
        graph.removeAllEdges(graph.edgesOf(graph.getVertex("M_sarcs_c")));
        //create a compound
        graph.addVertex(new BioMetabolite("fake_compound"));
            //or: graph.addVertex(graph.createVertex());
        //create an edge
        ReactionEdge e = new ReactionEdge(
                graph.getVertex("M_sarcs_c"),
                graph.getVertex("fake_compound"),
                new BioReaction("fake_reaction"));
        graph.addEdge(e);
            //or simply: graph.createEdge(u,v), if the reaction is irrelevant;
        //invert direction of an edge
        graph.reverseEdge(graph.getEdge("M_sarcs_c","fake_compound","fake_reaction"));
        //remove edges from a reaction
        graph.removeAllEdges(graph.getEdgesFromReaction("fake_reaction"));
        //remove a compound and its incident edges
        graph.removeVertex(graph.getVertex("M_sarcs_c"));
        //remove isolated nodes
        graph.removeIsolatedNodes();
        System.err.println("nodes : "+graph.vertexSet().size());
        System.err.println("edges : "+graph.edgeSet().size());

        /*
        III. ANALYZING METABOLIC GRAPH
        ===============================

        Exporting a Metabolic Graph in standard graph format
        ----------------------------------------------------
        The following code exports the compound graph as a GML file.
        GML is supported by a wide range of graph analysis tools, which will allow visualizing and analyzing metabolic networks structure,
        using external software such as Cytoscape, or graph library in other language such as python/R igraph, for example.
         */
        System.out.println("exporting graph...");
        ExportGraph.toGmlWithAttributes(graph, outputDir+"metabolic_graph.gml");
            //Other formats are available from the ExportGraph class.
            //The GML contains labels and attributes that are commonly relevant, such as masses or formulas for nodes.


        /*
        Exporting a Metabolic Graph as an adjacency matrix
        --------------------------------------------------
         */
        System.out.println("exporting matrix...");
        ComputeAdjacencyMatrix adjBuilder = new ComputeAdjacencyMatrix(graph);
        //if the metabolic graph has parallel edges, the merge can be set using the following line:
        //```
        //adjBuilder.parallelEdgeWeightsHandling((u, v) -> Math.min(u,v));
        //```
        ExportMatrix.toCSV(outputDir+"metabolic_graph_matrix.csv",adjBuilder.getadjacencyMatrix());


        /*
        Exploit Graph using JGrapht
        ---------------------------
        While metabolic graphs produced by Met4J can be used externally through the previous export,
        all the BioGraph are compatible with the JGrapht library algorithms, and can be directly analyzed in Java.
        Examples and documentation for JGrapht can be found at https://jgrapht.org.
         */

        System.out.println("analyzing graph...");
        //EXAMPLES:
        //return number of connected components:
        ConnectivityInspector<BioMetabolite, ReactionEdge> connex = new ConnectivityInspector<>(graph);
        List<Set<BioMetabolite>> cc = connex.connectedSets();
        System.err.println("number of connected components : "+cc.size());

    }

}
